const sync = require('../controllers/sync');

const syncUsers = (userRepository) => async (req, res) => { // eslint-disable-line
  const { status, data } = await sync(userRepository);
  res.status(status).send(data);
};

module.exports = syncUsers;
