const axios = require('axios');

const sync = async (userRepository) => {
  try {
    const { data, status } = await axios.get('https://api.github.com/search/users?q=payflow');
    const usersList = userRepository
      .findAllUsers()
      .map((item) => (item.userName));

    const editedList = [];
    const response = {
      inserted: 0,
      updated: 0,
      deleted: 0,
    };
    if (status <= 400) {
      data.items.forEach((item) => {
        const {
          login, id, type, avatar_url, // eslint-disable-line camelcase
        } = item;
        if (type === 'User') {
          const user = {
            userName: login,
            externalId: id.toString(),
            picture: avatar_url,
          };
          if (!usersList.includes(login)) {
            userRepository.addUser(user);
            response.inserted += 1;
          } else {
            userRepository.updatedUser(user);
            response.updated += 1;
          }
          editedList.push(login);
        }
      });
    }
    const diff = usersList.filter((x) => (!editedList.includes(x)));

    diff.forEach((item) => {
      userRepository.deleteUserByUserName(item);
      response.deleted += 1;
    });

    return { status: 200, data: response };
  } catch (e) {
    throw new Error(e);
  }
};

module.exports = sync;
