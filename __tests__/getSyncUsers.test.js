const request = require('supertest');
const app = require('../infrastructure/server');

describe('Get Sync Users', () => {
  it('should return report', async () => {
    const res = await request(app.server)
      .get('/users/sync');

    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveProperty('inserted');
    expect(res.body).toHaveProperty('updated');
    expect(res.body).toHaveProperty('deleted');
  });
});
